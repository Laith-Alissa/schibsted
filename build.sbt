name := "schibsted"
version := "0.1"
scalaVersion := "2.12.2"

mainClass in (Compile, packageBin) := Some("com.laithalissa.schibsted.Main")
artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "." + artifact.extension
}

target in assembly := target.value
assemblyJarName in assembly := "schibsted.jar"

// META-INF discarding
mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
  }
}

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-compiler" % "2.12.2"
)
