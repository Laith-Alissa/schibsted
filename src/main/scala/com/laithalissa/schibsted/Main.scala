import java.io.File
import scala.io.BufferedSource
import scala.io.Source
import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.ILoop

trait FileSearching {
  def loadFile(filePath: String): BufferedSource = Source.fromFile(filePath)

  def searchFile(filePath: String, searchTerm: String): Boolean = {
    loadFile(filePath).getLines.mkString(" ").contains(searchTerm)
  }

  def doSearch(filePaths: Seq[String], searchTerm: String): Map[String, Int] = {
    filePaths.collect {
      case filePath: String if searchFile(filePath, searchTerm) => filePath -> 100
    }.toMap
  }
}

class FileSearchREPL(
  targetDir: String,
  filesFound: Seq[String]
) extends ILoop with FileSearching {
  override def prompt = "search> "

  override def processLine(line: String): Boolean = {
    doSearch(filesFound, line) map {
      case (filename, score) => println(s"$filename: $score")
    }
    true
  }

  override def printWelcome() {
    echo(s"${filesFound.length} files read in directory $targetDir")
  }
}

object Main {

  private def findFilesOnPath(path: String): Seq[String] = {
    new File(path).listFiles().toSeq collect { case f if f.isFile => f.getCanonicalPath() }
  }

  def main(args: Array[String]): Unit = {
    val settings = new Settings
    settings.usejavacp.value = true
    settings.deprecation.value = true

    if (args.isEmpty) {
      throw new RuntimeException("You must supply the target directory")
    }
    val dirPath = args(0)
    val filesFound = findFilesOnPath(dirPath)
    new FileSearchREPL(targetDir = dirPath, filesFound = filesFound).process(settings)
  }
}

