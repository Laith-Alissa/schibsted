FROM hseeberger/scala-sbt
VOLUME /mount/data
VOLUME /usr/src/app
WORKDIR /usr/src/app
# TODO: Swap over volume mount to copy
# COPY . .
RUN sbt package
ENTRYPOINT ["java", "-jar", "target/schibsted.jar"]
