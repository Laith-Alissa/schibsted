#!/bin/bash
docker build -t schibsted-test:latest . &&
docker run \
  -v "$(pwd)":/usr/src/app \
  --rm -it \
  schibsted-test:latest
